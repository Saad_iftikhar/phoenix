apiVersion: v1
kind: ConfigMap
metadata:
  creationTimestamp: null
  name: prometheus-rules-{{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
data:
  node-down.rules: |
    groups:
    - name: node-down.rules
      rules:
      - alert: NodeDown
        expr: up{job="kubernetes-nodes"} == 0
        for: 10m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }}$labels.kubernetes_io_hostname{{ "}}" }}: Node has been down for more than 10 minutes'
          SUMMARY: '{{ "{{" }}$labels.kubernetes_io_hostname{{ "}}" }}: Node Down'

  node-memory-usage.rules: |
    groups:
    - name: node-memory-usage.rules
      rules:
      - alert: NodeMemoryUsage
        expr: ((node_memory_MemTotal_bytes - node_memory_MemAvailable_bytes) / node_memory_MemTotal_bytes) * 100 > 95
        for: 5m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }}$labels.instance{{ "}}" }}: Node memory usage has exceeded 95 percent(current memory usage percentage is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: '{{ "{{" }}$labels.instance{{ "}}" }}: Node memory usage has exceeded 95 percents'

  vm-down.rules: |
    groups:
    - name: vm-down.rules
      rules:
      - alert: VmDown
        expr: libvirt_domain_active == 0
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }} $labels.instance {{ "}}" }}: VM {{ "{{" }} $labels.domain {{ "}}" }} of {{ "{{" }} $labels.node {{ "}}" }} has been down for more than 1 minute'
          SUMMARY: '{{ "{{" }} $labels.instance {{ "}}" }}: VM Down'

  low-disk-hdd.rules: |
    groups:
    - name: low-disk-hdd.rules
      rules:
      - alert: NodeLowDisk
        expr: (node_filesystem_size_bytes - node_filesystem_avail_bytes) / node_filesystem_size_bytes
          * 100 > 80
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }}$labels.device{{ "}}" }}: Node disk usage has exceeded 80 percent, for mountpoint: {{ "{{" }}$labels.mountpoint{{ "}}" }} (current value is: {{ "{{" }}$value {{ "}}" }})'
          SUMMARY: '{{ "{{" }}$labels.device{{ "}}" }}: Node disk usage has exceeded 80 percent'
      - alert: NodeLowDiskCritical
        expr: (node_filesystem_size_bytes - node_filesystem_avail_bytes) / node_filesystem_size_bytes
          * 100 > 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }}$labels.device{{ "}}" }}: Node disk usage has exceeded 95 percent, for mountpoint: {{ "{{" }}$labels.mountpoint{{ "}}" }} (current value is: {{ "{{" }} $value {{ "}}" }})
          This is an urgent Alert and require immediate attention'
          SUMMARY: '{{ "{{" }}$labels.device{{ "}}" }}: Node disk usage has exceeded 95 percent'

  pod-restart.rules: |
    groups:
    - name: pod-restart.rules
      rules:
      - alert: PodMultipleRestart
        expr: changes(kube_pod_container_status_restarts_total[5m]) > 2
        for: 5m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }}$labels.pod{{ "}}" }}: Pod of {{ "{{" }}$labels.namespace{{ "}}" }} namespace is continuously failing and restarting (current restart count is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: '{{ "{{" }}$labels.pod{{ "}}" }}: Pod is continuously failing and restarting.'

  calypso.rules: |
    groups:
    - name: calypso.rules
      rules:
      - record: calypso:requests_total:rate1m
        expr: sum(rate(calypso_requests_total[1m]))
      - record: calypso:requests_total_success:rate1m
        expr: sum(rate(calypso_requests_total{result="SUCCESS"}[1m]))
      - record: calypso:requests_total_failure:rate1m
        expr: sum(rate(calypso_requests_total{result="FAILURE"}[1m]))
      - record: calypso:requests_total:method:result:rate1m
        expr: sum(rate(calypso_requests_total[1m])) by (method, result)
      - record: calypso:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(calypso_requests_latency_milliseconds_bucket[1m])))
      - record: calypso:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(calypso_requests_latency_milliseconds_bucket[1m])))
      - record: calypso:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(calypso_requests_latency_milliseconds_bucket[1m])))
      - record: calypso:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(calypso_requests_latency_milliseconds_bucket[1m])))
      - record: calypso:sli:success_percentage:30d
        expr: sum(rate(calypso_requests_total{result="SUCCESS"}[30d])) / sum(rate(calypso_requests_total[30d])) * 100
      - alert: CalypsoSliSuccessPercentage
        expr: calypso:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Calypso SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: 'Calypso SLI success percentage has dropped below 95 percent.'

  calypso-db.rules: |
    groups:
    - name: calypso-db.rules
      rules:
      - record: calypso_db:requests_total:rate1m
        expr: sum(rate(calypso_db_requests_total[1m]))
      - record: calypso_db:requests_total_success:rate1m
        expr: sum(rate(calypso_db_requests_total{result="SUCCESS"}[1m]))
      - record: calypso_db:requests_total_failure:rate1m
        expr: sum(rate(calypso_db_requests_total{result="FAILURE"}[1m]))
      - record: calypso_db:requests_total:method:result:rate1m
        expr: sum(rate(calypso_db_requests_total[1m])) by (method, result)
      - record: calypso_db:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(calypso_db_requests_latency_milliseconds_bucket[1m])))
      - record: calypso_db:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(calypso_db_requests_latency_milliseconds_bucket[1m])))
      - record: calypso_db:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(calypso_db_requests_latency_milliseconds_bucket[1m])))
      - record: calypso_db:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(calypso_db_requests_latency_milliseconds_bucket[1m])))
      - record: calypso_db:sli:success_percentage:30d
        expr: sum(rate(calypso_db_requests_total{result="SUCCESS"}[30d])) / sum(rate(calypso_db_requests_total[30d])) * 100
      - alert: CalypsoDbSliSuccessPercentage
        expr: calypso_db:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'CalypsoDB SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: 'CalypsoDB SLI success percentage has dropped below 95 percent.'

  iris.rules: |
    groups:
    - name: iris.rules
      rules:
      - record: iris:requests_total:rate1m
        expr: sum(rate(iris_requests_total[1m])) by (method, result)
      - record: iris:requests_total:method:rate1m
        expr: sum(rate(iris_requests_total[1m])) by (method)
      - record: iris:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(iris_requests_latency_milliseconds_bucket[1m])))
      - record: iris:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(iris_requests_latency_milliseconds_bucket[1m])))
      - record: iris:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(iris_requests_latency_milliseconds_bucket[1m])))
      - record: iris:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(iris_requests_latency_milliseconds_bucket[1m])))
      - record: iris:sli:success_percentage:30d
        expr: sum(rate(iris_requests_total{result="SUCCESS"}[30d])) / sum(rate(iris_requests_total[30d])) * 100
      - alert: IrisSliSuccessPercentage
        expr: iris:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Iris SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }}$value {{ "}}" }})'
          SUMMARY: 'Iris SLI success percentage has dropped below 95 percent.'

  pathfinder.rules: |
    groups:
    - name: pathfinder.rules
      rules:
      - record: pathfinder:api_calls_total:method
        expr: sum(pathfinder_api_calls_total) by (method)
      - record: pathfinder:published_messages_total:rate1m
        expr: sum(rate(pathfinder_published_messages_total[1m]))
      - record: pathfinder:published_messages_total:topic:rate1m
        expr: sum(rate(pathfinder_published_messages_total[1m])) by (topic)
      - record: pathfinder:read_messages_total:topic:rate1m
        expr: sum(rate(pathfinder_read_messages_total[1m])) by (topic)
      - record: pathfinder:read_messages_total:result:rate1m
        expr: sum(rate(pathfinder_read_messages_total[1m])) by (result)
      - record: pathfinder:filter_messages_permitted_total:rate1m
        expr: sum(rate(pathfinder_filter_messages_permitted_total[1m]))
      - record: pathfinder:filter_messages_dropped_total:rate1m
        expr: sum(rate(pathfinder_filter_messages_dropped_total[1m]))

  phoenix_publish.rules: |
    groups:
    - name: phoenix_publish.rules
      rules:
      - record: phoenix_publish:input_operation_total:rate1m
        expr: sum(rate(phoenix_publish_input_operation_total[1m]))
      - record: phoenix_publish:input_operation_total:topic:rate1m
        expr: sum(rate(phoenix_publish_input_operation_total[1m])) by (topic)
      - record: phoenix_publish:output_operation_total:rate1m
        expr: sum(rate(phoenix_publish_output_operation_total[1m]))
      - record: phoenix_publish:output_operation_success:rate1m
        expr: sum(rate(phoenix_publish_output_operation_total{result="success"}[1m]))
      - record: phoenix_publish:output_operation_failure:rate1m
        expr: sum(rate(phoenix_publish_output_operation_total{result=~"malformed.+"}[1m]))
      - record: phoenix_publish:output_element_total:rate1m
        expr: sum(rate(phoenix_publish_output_element_total[1m]))
      - record: phoenix_publish:bundle_processing_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_publish_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_publish_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_publish_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_publish_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_entry_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_publish_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_entry_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_publish_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_entry_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_publish_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:bundle_entry_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_publish_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:element_processing_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_publish_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:element_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_publish_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:element_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_publish_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:element_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_publish_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_publish:element_entry_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_publish_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_publish:element_entry_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_publish_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_publish:element_entry_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_publish_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_publish:element_entry_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_publish_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_publish:sli:output_operation_success_percentage:rate30d
        expr: sum(rate(phoenix_publish_output_operation_total{result="success"}[30d])) / sum(rate(phoenix_publish_output_operation_total[30d])) * 100
      - record: phoenix_publish:sli:operation_processing_percentage:rate30d
        expr: sum(rate(phoenix_publish_input_operation_total[30d])) / sum(rate(phoenix_publish_output_operation_total[30d])) * 100
      - alert: PhoenixPublishSliSuccessPercentage
        expr: phoenix_publish:sli:output_operation_success_percentage:rate30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Phoenix-Publish SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: 'Phoenix-Publish SLI success percentage has dropped below 95 percent.'

  phoenix_historize.rules: |
    groups:
    - name: phoenix_historize.rules
      rules:
      # input
      - record: phoenix_historize:input_operation_total:rate1m
        expr: sum(rate(phoenix_historize_input_operation_total[1m]))
      - record: phoenix_historize:input_operation_total:topic:rate1m
        expr: sum(rate(phoenix_historize_input_operation_total[1m])) by (topic)
      - record: phoenix_historize:output_element_total:rate1m
        expr: sum(rate(phoenix_historize_output_element_total[1m]))
      - record: phoenix_historize:output_operation_total:rate1m
        expr: sum(rate(phoenix_historize_output_operation_total[1m]))
      - record: phoenix_historize:output_operation_success:rate1m
        expr: sum(rate(phoenix_historize_output_operation_total{result="success"}[1m]))
      - record: phoenix_historize:output_operation_failure:rate1m
        expr: sum(rate(phoenix_historize_output_operation_total{result!="success"}[1m]))
      - record: phoenix_historize:bundle_processing_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_historize_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_historize_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_historize_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_historize_bundle_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_entry_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_historize_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_entry_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_historize_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_entry_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_historize_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:bundle_entry_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_historize_bundle_entry_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:element_processing_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_historize_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:element_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_historize_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:element_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_historize_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:element_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_historize_element_processing_duration_milliseconds_bucket[1m])) by (le))
      - record: phoenix_historize:element_entry_duration:50percentile
        expr: histogram_quantile(0.50, sum(rate(phoenix_historize_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_historize:element_entry_duration:75percentile
        expr: histogram_quantile(0.75, sum(rate(phoenix_historize_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_historize:element_entry_duration:95percentile
        expr: histogram_quantile(0.95, sum(rate(phoenix_historize_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_historize:element_entry_duration:99percentile
        expr: histogram_quantile(0.99, sum(rate(phoenix_historize_element_entry_duration_millisecons_bucket[1m])) by (le))
      - record: phoenix_historize:sli:output_operation_success_percentage:rate30d
        expr: sum(rate(phoenix_historize_output_operation_total{result="success"}[30d])) / sum(rate(phoenix_historize_output_operation_total[30d])) * 100
      - record: phoenix_historize:sli:operation_processing_percentage:rate30d
        expr: sum(rate(phoenix_historize_input_operation_total[30d])) / sum(rate(phoenix_historize_output_operation_total[30d])) * 100
      - alert: PhoenixHistorizeSuccessPercentage
        expr: phoenix_historize:sli:output_operation_success_percentage:rate30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Phoenix-Historize SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }} $value {{ "}}" }})'
          SUMMARY: 'Phoenix-Historize SLI success percentage has dropped below 95 percent.'

  cache_service.rules: |
    groups:
    - name: cache_service.rules
      rules:
      - record: cache_service_requests_total:rate1m
        expr: sum(rate(cache_service_requests_total[1m]))
      - record: cache_service_requests_total:namespace:rate1m
        expr: sum(rate(cache_service_requests_total[1m])) by (namespace)
      - record: cache_service_requests_total:method:rate1m
        expr: sum(rate(cache_service_requests_total[1m])) by (method)
      - record: cache_services_reqeust_total:result:rate1m
        expr: sum(rate(cache_service_requests_total[1m])) by (result)
      - record: cache_service:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(cache_service_requests_latency_milliseconds_bucket[1m])))
      - record: cache_service:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(cache_service_requests_latency_milliseconds_bucket[1m])))
      - record: cache_service:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(cache_service_requests_latency_milliseconds_bucket[1m])))
      - record: cache_service:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(cache_service_requests_latency_milliseconds_bucket[1m])))
      - record: cache_service:sli:success_percentage:30d
        expr: sum(rate(cache_service_requests_total{result=~"ILLEGAL_NAMESPACE_FAILURE|.+SUCCESS"}[30d]))
          / sum(rate(cache_service_requests_total[30d])) * 100
      - alert: CacheServiceSliSuccessPercentage
        expr: cache_service:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Cache-Service SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }}$value {{ "}}" }})'
          SUMMARY: 'Cache-Service SLI success percentage has dropped below 95 percent.'

  airship.rules: |
    groups:
    - name: airship.rules
      rules:
      - record: airship:requests_total:label:rate1m
        expr: sum by(method) (rate(airship_request_total{method!="database_query"}[1m]))
      - record: airship:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le, method) (rate(airship_request_latency_milliseconds_bucket[1m])))
      - record: airship:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le, method) (rate(airship_request_latency_milliseconds_bucket[1m])))
      - record: airship:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le, method) (rate(airship_request_latency_milliseconds_bucket[1m])))
      - record: airship:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le, method) (rate(airship_request_latency_milliseconds_bucket[1m])))
      - record: airship:sli:success_percentage:30d
        expr: (sum(rate(airship_request_total{status="200 OK"}[30d])) / sum(rate(airship_request_total[30d]))) * 100
      - alert: AirshipSliSuccessPercentage
        expr: airship:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Airship SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }}  $value {{ "}}" }})'
          SUMMARY: 'Airship SLI success percentage has dropped below 95 percent.'

  atlas.rules: |
    groups:
    - name: atlas.rules
      rules:
      - record: atlas:requests_total:rate1m
        expr: sum(rate(atlas_requests_total[1m]))
      - record: atlas:request_total:method:rate1m
        expr: sum(rate(atlas_requests_total[1m])) by (method)
      - record: atlas:requests_total:result:rate1m
        expr: sum(rate(atlas_requests_total[1m])) by (result)
      - record: atlas:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(atlas_requests_latency_milliseconds_bucket[1m])))
      - record: atlas:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(atlas_requests_latency_milliseconds_bucket[1m])))
      - record: atlas:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(atlas_requests_latency_milliseconds_bucket[1m])))
      - record: atlas:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(atlas_requests_latency_milliseconds_bucket[1m])))
      - record: atlas:data_received:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(atlas_data_received_bucket[1m])))
      - record: atlas:data_received:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(atlas_data_received_bucket[1m])))
      - record: atlas:data_received:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(atlas_data_received_bucket[1m])))
      - record: atlas:data_received:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(atlas_data_received_bucket[1m])))
      - record: atlas:sli:success_percentage:30d
        expr: sum(rate(atlas_requests_total{result = "SUCCESS"}[30d]))/sum(rate(atlas_requests_total[30d])) * 100
      - alert: AtlasSliSuccessPercentage
        expr: atlas:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Atlas SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }}  $value {{ "}}" }})'
          SUMMARY: 'Atlas SLI success percentage has dropped below 95 percent.'

  orion.rules: |
    groups:
    - name: orion.rules
      rules:
      - record: orion:requests_total:label:rate1m
        expr: sum by(method, result) (rate(orion_requests_total[1m]))
      - record: orion:requests_usage_total
        expr: sum by(method) (orion_requests_total)
  
  cleopytra.rules: |
    groups:
    - name: cleopytra.rules
      rules:
      - record: cleopytra:input_bundle_total:rate1m
        expr: sum(rate(cleopytra_input_bundle_total[1m]))
      - record: cleopytra:output_bundle_total:rate1m
        expr: sum(rate(cleopytra_output_bundle_total[1m]))
      - record: cleopytra:output_bundle_total:airship_service_name:type:rate1m
        expr: sum by(airship_service_name, type) (rate(cleopytra_output_bundle_total[1m]))
      - record: cleopytra:output_bundle_total:airship_service_name:rate1m
        expr: sum by(airship_service_name) (rate(cleopytra_output_bundle_total[1m]))
      - record: cleopytra:input_total:rate1m
        expr: sum(rate(cleopytra_input_total[1m]))
      - record: cleopytra:output_total:rate1m
        expr: sum(rate(cleopytra_output_total[1m]))
      - record: cleopytra:output_total:airship_service_name:type:rate1m
        expr: sum by(airship_service_name, type) (rate(cleopytra_output_total[1m]))
      - record: cleopytra:output_total:airship_service_name:rate1m
        expr: sum by(airship_service_name) (rate(cleopytra_output_total[1m]))
      - record: cleopytra:requests_total:rate1m
        expr: sum((rate(cleopytra_requests_total[1m])))
      - record: cleopytra:requests_total:airship_service_name:method:result:rate1m
        expr: sum by(airship_service_name, method, result) (rate(cleopytra_requests_total[1m]))
      - record: cleopytra:requests_total:airship_service_name:rate1m
        expr: sum by(airship_service_name) (rate(cleopytra_requests_total[1m]))
      - record: cleopytra:request_processing_duration:50percentile
        expr: histogram_quantile(0.5, sum by (le, airship_service_name) (rate(cleopytra_request_latency_ms_bucket[1m])))
      - record: cleopytra:request_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum by (le, airship_service_name) (rate(cleopytra_request_latency_ms_bucket[1m])))
      - record: cleopytra:request_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum by (le, airship_service_name) (rate(cleopytra_request_latency_ms_bucket[1m])))
      - record: cleopytra:request_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum by (le, airship_service_name) (rate(cleopytra_request_latency_ms_bucket[1m])))
      - record: cleopytra:element_processing_duration:50percentile
        expr: histogram_quantile(0.5, sum  by (le, airship_service_name) (rate(cleopytra_element_latency_ms_bucket[1m])))
      - record: cleopytra:element_processing_duration:75percentile
        expr: histogram_quantile(0.75, sum  by (le, airship_service_name) (rate(cleopytra_element_latency_ms_bucket[1m])))
      - record: cleopytra:element_processing_duration:95percentile
        expr: histogram_quantile(0.95, sum  by (le, airship_service_name) (rate(cleopytra_element_latency_ms_bucket[1m])))
      - record: cleopytra:element_processing_duration:99percentile
        expr: histogram_quantile(0.99, sum  by (le, airship_service_name) (rate(cleopytra_element_latency_ms_bucket[1m])))
      - record: cleopytra:publish_duration:50percentile
        expr: histogram_quantile(0.5, sum  by (le, airship_service_name) (rate(cleopytra_publish_latency_ms_bucket[1m])))
      - record: cleopytra:publish_duration:75percentile
        expr: histogram_quantile(0.75, sum  by (le, airship_service_name) (rate(cleopytra_publish_latency_ms_bucket[1m])))
      - record: cleopytra:publish_duration:95percentile
        expr: histogram_quantile(0.95, sum  by (le, airship_service_name) (rate(cleopytra_publish_latency_ms_bucket[1m])))
      - record: cleopytra:publish_duration:99percentile
        expr: histogram_quantile(0.99, sum  by (le, airship_service_name) (rate(cleopytra_publish_latency_ms_bucket[1m])))        

  valkyrie.rules: |
    groups:
    - name: valkyrie.rules
      rules:
      - record: valkyrie:requests_total:rate1m
        expr: sum(rate(valkyrie_http_requests_total[1m]))
      - record: valkyrie:requests_total:request_uri:rate1m
        expr: sum(rate(valkyrie_http_requests_total[1m])) by (request_uri)
      - record: valkyrie:requests_total:status:rate1m
        expr: sum(rate(valkyrie_http_requests_total[1m])) by (status)
      - record: valkyrie:request_latency:50percentile
        expr: histogram_quantile(0.5, sum by(le) (rate(valkyrie_http_requests_latency_seconds_bucket[1m])))
      - record: valkyrie:request_latency:75percentile
        expr: histogram_quantile(0.75, sum by(le) (rate(valkyrie_http_requests_latency_seconds_bucket[1m])))
      - record: valkyrie:request_latency:95percentile
        expr: histogram_quantile(0.95, sum by(le) (rate(valkyrie_http_requests_latency_seconds_bucket[1m])))
      - record: valkyrie:request_latency:99percentile
        expr: histogram_quantile(0.99, sum by(le) (rate(valkyrie_http_requests_latency_seconds_bucket[1m])))
      - record: valkyrie:sli:success_percentage:30d
        expr: (sum(rate(valkyrie_http_requests_total{status!~".*ERROR"}[30d])) / sum(rate(valkyrie_http_requests_total[30d]))) * 100
      - alert: ValkyrieSliSuccessPercentage
        expr: valkyrie:sli:success_percentage:30d < 95
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Valkyrie SLI success percentage has dropped below 95 percent(current SLI percentage is: {{ "{{" }}  $value {{ "}}" }})'
          SUMMARY: 'Valkyrie SLI success percentage has dropped below 95 percent.'

  raid.rules: |
    groups:
    - name: raid.rules
      rules:
      - alert: RaidAlert
        expr: raid_status == 0
        for: 1m
        labels:
          severity: page
        annotations:
          DESCRIPTION: '{{ "{{" }} $labels.node {{ "}}" }}: Raid {{ "{{" }}  $labels.device_name {{ "}}" }} device type {{ "{{" }}  $labels.device_type {{ "}}" }}on node {{ "{{" }}  $labels.node {{ "}}" }} status is:  {{ "{{" }}  $labels.device_status {{ "}}" }}'
          SUMMARY: '{{ "{{" }}  $labels.node {{ "}}" }}: Raid {{ "{{" }}  $labels.device_name {{ "}}" }} status is: {{ "{{" }}  $labels.device_status {{ "}}" }}'
  kepmon.rules: |
    groups:
    - name: kepmon.rules
      rules:
      - alert: KepwareFailover
        expr: kepmon_kepserver_health{status="Unhealthy"} > 0
        for: 30s
        labels:
          severity: page
        annotations:
          DESCRIPTION: 'Kepware Failed {{ "{{" }}  $labels.kepserver {{ "}}" }}'
          SUMMARY: 'kepware:{{ "{{" }}  $labels.kepserver {{ "}}" }} has failed, status: {{ "{{" }}  $labels.status {{ "}}" }}'
