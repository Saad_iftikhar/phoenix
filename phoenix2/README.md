# Argo CD Helm Chart

Helm chart for Phoenix service.

## Chart Version
### 1.0.38

## Prerequisites
- Kubernetes installed on cluster.
- Kubectl access to the cluster.
- Helm V3 installed on the local machine

## Installing the Chart

To install the chart with release name "version1" :

```console
$ helm repo add helm https://intechww.jfrog.io/artifactory/helm --username $$JFROG_USERNAME --password $$JFROG_PASSWORD
"helm" has been added to your repositories

$ helm install --name version1 phoenix/
```

## Chart Values 

Variables with *Compulsory* field `no` have default values present in the chart.

 Parameter | Description | Compulsory |
|-----|------|---------|
| replicaCount| Number of pods for the phoenix service | `Yes` |
| site | Site of the clsuter; 'onprem' or 'cloud' | `no` |
| resource.requests.phoenixCpus | Cpu resources for the phoenix pods (requests) | `no` |
| resource.requests.phoenixMem | Memory resources for the phoenix pods (requests) | `no` |
| resource.limits.phoenixCpus | Cpu resources for the phoenix pods (limits) | `no` |
| resource.limits.phoenixMem | Memory resources for the phoenix pods (limits) | `no` |
| clusterName | Name of the Kubernetes cluster| `yes` |
| phoenixVolumeDiskSize | Disk sise on the onprem machine where phoenix data is to be stored | `yes` |
| localVolumeHostPath| Path on the onprem machines where data for phoenix volume to be stored | `yes` |
| historian.hostname | Name of the historian service in the cluster | `yes` |
| historian.port | Port no of the historian service running in the cluster | `yes` |
| historian.serverName | If the `site` is *azure* then cloud historian server name | `no` |

## Dependencies

- stolon
    - version >= 0.16.0
    - repository: https://intechww.jfrog.io/artifactory/helm

## Architectur Layer

Phoenix is a [Layer-3](https://bitbucket.org/intechww/docs/src/c00455bfe0d66bcbff0c2284374f2c69a28b0550/processes/backend/stack.md) service in our architecture stack and hence will be deployed through GitOps using Argo CD.


