# Helm Chart for Rook opeator

This is helm chart for deploying Rook opeator. [Rook](https://github.com/rook/rook) is a cloud-native storage orchestrator for Kubernetes providing support for cloud native storage solutions; in our case Ceph.

## Prerequisites

- Kubernetes cluster set up.
- Kubectl access to the Kubernetes cluster setup.
- Helm V3 installed on your local system.

## Installing the Chart from inside helm repo

To install the chart with release name `version1`:

```console
helm install --name version1 rook/
```

## Dependencies

No dependencies on other helm charts.

## Architecture layer

Rook is a [Layer-2](https://bitbucket.org/intechww/docs/src/c00455bfe0d66bcbff0c2284374f2c69a28b0550/processes/backend/stack.md) service in our architecture and hence will be deployed through ansible playbooks or helm install command as mentioned above.
